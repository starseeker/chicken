(define-constant banner #<<EOF

      /    /      /             
 ___ (___    ___ (     ___  ___ 
|    |   )| |    |___)|___)|   )
|__  |  / | |__  | \  |__  |  / 


EOF
)

(define-constant copyright "(c)2000-2007 Felix L. Winkelmann")
