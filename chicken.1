.\" dummy line
.TH CHICKEN 1 "10 Sep 2002"

.SH NAME

Chicken \- A Scheme\-to\-C compiler

.SH SYNOPSIS

.B chicken
.I pathname
[
.I option ...
]

.SH DESCRIPTION

.I Chicken
is a compiler for the programming language
.I Scheme
supporting most of the features as described in the
.I Revised^5 Report on
.I the Algorithmic Language Scheme
\.

.SH OPTIONS

.TP
.B \-analyze\-only
Stop compilation after first analysis pass.

.B \-benchmark\-mode
Equivalent to
.B \-optimize\-level\ 3\ \-fixnum\-arithmetic\ \-disable\-interrupts\ \-lambda\-lift
.B \-block\ \-no\-lambda\-info

.TP
.B \-block
Enable block-compilation. When this option is specified, the compiler assumes that global variables are not modified outside this compilation-unit.

.TP
.B \-case\-insensitive
Enables the reader to read symbols case-insensitive. The default is to read case-sensitive (in violation of R5RS).
This option registers the
.B case\-insensitive
feature identifier.

.TP
.B \-check\-imports
Search for references to undefined global variables.

.TP
.B \-check\-syntax
Aborts compilation process after macro-expansion and syntax checks.

.TP
.B \-compress\-literals\ threshold
Compiles quoted literals that exceed the size 
.BI threshold
as strings
and parse the strings at run-time. This reduces the size of the code and
speeds up compile-times of the host C compiler, but has a small run-time
performance penalty. The size of a literal is computed by counting recursively the objects
in the literal, so a vector counts as 1 plus the count of the elements,
a pair counts as the counts of the car and the cdr, respectively.
All other objects count 1.

.TP
.BI \-database\-size \ number
Specifies the initial size of the analysis-database. Should only be used if extremely large files are to be compiled.

.TP
.BI \-debug \ modes
Enables one or more debugging modes. See the User's Manual for more information.

.TP
.BI \-debug\-level \ level
Selects amount of debug-information. 
.I level
should be an integer.
.P
.br
.B \ \ \ \ 0
-no-trace -no-lambda-info
.br
.B \ \ \ \ 1
-no-trace
.br
.B \ \ \ \ 2
nothing.

.TP
.B \-disable\-c\-syntax\-checks
Disable basic syntax checking of embedded C code fragments.

.TP
.B \-disable\-interrupts
Equivalent to
.B \-prelude\ "(declare\ (interrupts-disabled))"

.TP
.B \-disable\-stack\-overflow\-checks
Disables detection of stack-overflows.

.TP \-disable\-warning\ class
Disables specific class of warnings, may be given multiple times.

.TP
.B \-dynamic
This option should be used when compiling files intended to be loaded dynamically into
a running Scheme program.

.TP
.BI \-epilogue \ filename
Includes the file named
.I filename
at the end of the compiled source file. 
The include-path is not searched. This option may be given multiple times.

.TP
.I \-emit\-debug\-info
Emit additional information for each 
.B lambda
expression (currently the argument-list,
after alpha-conversion/renaming).

.TP
.BI \-emit\-exports\ filename
Write exported toplevel variables to file
.B filename

.TP
.I \-emit\-external\-prototypes\-first
Emit prototypes for callbacks defined with 
.B define\-external 
before any
other foreign declarations. This is sometimes useful, when C/C++ code embedded into
the a Scheme program has to access the callbacks. By default the prototypes are emitted
after foreign declarations.

.TP
.B \-explicit\-use
Disables automatic use of the units
.I library
and
.I eval
\. Use this option if compiling a library unit
instead of an application unit.

.TP
.BI \-extend \ filename
Loads a Scheme file before compilation commences. This feature can be used to extend the compiler.

.TP
.B \-extension
Mostly equivalent to 
.B \-prelude\ \'\(define-extension\ NAME\)\'
where 
.B NAME
is the basename of the currently compiled file. Note that if you want to compile a file
as a normal (dynamically loadable) extension library, you should also pass the
.I \-shared
option.

.TP
.BI \-feature \ symbol
Registers
.I symbol
to be a valid feature identifier for
.B cond\-expand

.TP
.B \-fixnum\-arithmetic
Equivalent to
.B \-prelude\ "(declare\ (fixnum))"

.TP
.BI \-heap\-size \ number
Sets the static heap-size of the generated executable to
.I number
bytes. The parameter may be
followed by a
.B M
or
.B K
suffix which stand for mega- and kilobytes, respectively. The default heap-size is 16 megabytes.

.TP
.BI \-heap\-initial\-size \ number
Sets the size that the heap of the compiled application should have at startup time.

.TP
.BI \-heap\-growth \ percentage
Sets the heap-growth rate for the compiled program at compile time.

.TP
.BI \-heap\-shrinkage \ percentage
Sets the heap-shrinkage rate for the compiled program at compile time.

.TP
.B \-help
Print a summary of available options and the format of the command-line parameters and exit the compiler.

.TP
.BI \-import\ pathname
Read exports from linked or loaded libraries from given file. Implies
.B \-check\-imports

.TP
.BI \-include\-path \ pathname
Specifies an additional search path for files included via the 
.I include
special form. This option may be given multiple times. If the environment variable
.B CHICKEN_INCLUDE_PATH
is set, it should contain a list of alternative include
pathnames separated by
.I \;
\.
The environment variable
.B CHICKEN_HOME
is also considered as a search path.

.TP
.B \-inline
Enables procedure inlining.

.TP
.BI \-inline\-limit threshold
Sets the maximum size of potentially inlinable procedures.

.TP
.BI \-keep\-shadowed\-macros
Do not remove macro definitions with the same name as assigned toplevel variables (the default is to remove the macro definition).

.TP
.BI \-keyword\-style style
Enables alternative keyword syntax, where style may be either
.B prefix
(as in Common Lisp), 
.B suffix
(as in DSSSL) or
.B none
Any other value is ignored. The default is \texttt{suffix}.

.TP
.B \-lambda\-lift
Enable the optimization known as lambda-lifting.

.TP
.B \-no\-trace
Disable generation of tracing information. If a compiled executable should halt due to a runtime error,
then a file containing a stack-trace will be written to the current directory under the name 
.I STACKTRACE
\. Each line in the created file gives the name and the line-number (if available) of a procedure call.
With this option given, the generated code is slightly faster.

.TP
.B \-no\-warnings
Disable generation of compiler warnings.

.TP
.BI \-nursery \ number
.TP
.BI \-stack\-size \ number
Sets the size of the first heap-generation of the generated executable to 
.I number
bytes. The parameter may
be followed by a
.B M
or
.B K
suffix. The default stack-size depends on the target platform.

.TP
.BI \-optimize\-leaf\-routines
Enable leaf routine optimization.

.TP
.BI \-optimize\-level \ level
Enables certain sets of optimization options. 
.I level
should be an integer. Each optimization level corresponds to a certain set of optimization option
as shown in the following list:
.P
.br
.B \ \ \ \ 0
nothing
.br
.B \ \ \ \ 1
-optimize-leaf-routines
.br
.B \ \ \ \ 2
-optimize-leaf-routines -usual-integrations
.br
.B \ \ \ \ 3
-optimize-leaf-routines -usual-integrations -unsafe

.TP
.BI \-output\-file \ filename
Specifies the pathname of the generated C file. Default is 
.I FILENAME.c
\.

.TP
.BI \-postlude \ expressions
Add
.I expressions
after all other toplevel expressions in the compiled file.
This option may be given multiple times. Processing of this option takes place after processing of
.BI \-epilogue
\.

.TP
.BI \-prelude \ expressions
Add
.I expressions
before all other toplevel expressions in the compiled file.
This option may be given multiple times. Processing of this option takes place before processing of
.B \-prologue
\.

.TP
.B \-profile
.B \-accumulate\-profile
Instruments the source code to count procedure calls and execution times. After the program terminates
(either via an explicit 
.B exit
or implicitly), profiling statistics are written to a file named
.B PROFILE
Each line of the generated file contains a list with the procedure name,
the number of calls and the time spent executing it. Use the script 
.B formatprofile
to display the profiling information in a more user-friendly form.

.TP
.BI \-prologue \ filename
Includes the file named 
.I filename
at the start of the compiled source file. 
The include-path is not searched. This option may be given multiple times.

.TP
.B \-quiet
Disables output of compile information.

.TP
.B \-release
Print release number and exit.

.TP
.BI \-require\-extension \ name
Loads the syntax-extension
.I name
before the source program is processed. This is identical to adding 
.B require\-extension\ NAME
at the start of
the compiled program.

.TP
.B \-run\-time\-macros
Makes low-level macros (defined with 
.B define\-macro
also available at run-time. By default
low-level macros are not available at run-time. Note that highlevel-macros ("syntax-case")
 defined in compiled code are never available at run-time.

.TP
.B \-to\-stdout
Write compiled code to standard output instead of creating a
.I .c
file.

.TP
.BI \-unit \ name
Compile this file as a library unit.

.TP
.B \-unsafe
Disable runtime safety checks.

.TP
.B \-unsafe\-libraries
Marks the generated file for being linked with the unsafe runtime system. This
should be used when generating shared object files that are to be loaded
dynamically. If the marker is present, any attempt to load code compiled with
this option will signal an error.

.TP
.BI \-uses \ name
Use definitions in the given library unit.

.TP
.B \-usual\-integrations
Specifies that standard procedures and certain internal procedures are never redefined, and can
be inlined. This is equivalent to declaring 
.I (usual\-integrations)
\.

.TP
.B \-verbose
Prints progress information to standard output during compilation.

.TP
.B \-version
Prints the version and some copyright information and exit the compiler.

.SH ENVIRONMENT\ VARIABLES

.TP
.B CHICKEN_HOME
Should contain the pathname where support- and include-files can be found.

.TP
.B CHICKEN_INCLUDE_PATH
Contains one or more pathnames where the compiler should additionally look for include-files, separated by 
.B \;
characters.

.TP
.B CHICKEN_OPTIONS
Holds a string of default compiler options that should apply to every invocation of
.B chicken
\.

.SH DOCUMENTATION

More information can be found in the
.I Chicken\ Manual
which is provided in HTML format as
.B manual\.html
in the distribution.

.SH BUGS

Submit bug reports by e-mail to
.I felix@call-with-current-continuation.org

.SH AUTHOR

.I Chicken
was implemented by Felix L. Winkelmann (felix@call-with-current-continuation.org).

.SH SEE ALSO

.BR csc(1)
.BR chicken-config(1)
